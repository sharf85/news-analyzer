package org.evgeny.newsanalyser;

import org.evgeny.newsanalyser.dto.NewsItemDto;
import org.evgeny.newsanalyser.model.NewsReport;
import org.evgeny.newsanalyser.service.IReportPublisher;
import org.evgeny.newsanalyser.service.NewsItemService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@SpringBootApplication
@MessageEndpoint
@EnableScheduling
public class NewsAnalyserApplication {

    public static void main(String[] args) {
        SpringApplication.run(NewsAnalyserApplication.class, args);
    }

    final NewsItemService itemService;
    final IReportPublisher reportService;

    public NewsAnalyserApplication(NewsItemService itemService, IReportPublisher reportService) {
        this.itemService = itemService;
        this.reportService = reportService;
    }

    @Value("${org.evgeny.newsanalizer.report.positiveitems}")
    int positiveItemsNum;

    /**
     * The method receives all the incoming News Items through the incoming tcp connections
     * @param newsItem item that comes in
     */
    @ServiceActivator(inputChannel = "fromTcpTransformed")
    public void receiveFromTcp(NewsItemDto newsItem) {
        itemService.handleItem(newsItem.headline, newsItem.priority);
    }

    /**
     * The method forms the reports and publishes them via
     */
    @Scheduled(fixedRateString = "${org.evgeny.newsanalizer.report.frequency}",
            initialDelayString = "${org.evgeny.newsanalizer.report.frequency}")
    public void publishReport() {
        NewsReport report = itemService.composeReport(positiveItemsNum);
        reportService.publish(report);
    }

}
