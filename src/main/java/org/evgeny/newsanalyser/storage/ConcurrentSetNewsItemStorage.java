package org.evgeny.newsanalyser.storage;

import org.evgeny.newsanalyser.model.NewsItem;

import java.util.*;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * The implementation has O(log(N)) complexity for adding and O(m) for getting m best elements
 */
public class ConcurrentSetNewsItemStorage implements INewsItemStorage {

    ConcurrentSkipListSet<NewsItem> source;

    AtomicInteger size;

    public ConcurrentSetNewsItemStorage(Comparator<NewsItem> comparator) {
        this.source = new ConcurrentSkipListSet<>(comparator);
        size = new AtomicInteger();
    }

    @Override
    public void add(NewsItem item) {
        /*
         * The complexity is O(log(N)), where N is the size of newsItems.
         */
        source.add(item);
        size.incrementAndGet();
    }

    @Override
    public List<NewsItem> getBestItems(int n) {
        /*
         * The complexity is O(n) approximately. n is the argument.
         */
        List<NewsItem> res = new ArrayList<>();

        // yes, this looks strange =) Since we do not filter the items while adding,
        // we do it here
        Set<Set<String>> headlines = new HashSet<>();
        Iterator<NewsItem> iterator = source.descendingIterator();

        while (iterator.hasNext() && res.size() < n) {
            NewsItem item = iterator.next();
            Set<String> headline = item.getHeadline();
            if (!headlines.contains(headline)) {
                headlines.add(headline);
                res.add(item);
            }
        }
        return res;
    }

    @Override
    public int getNumSeenItems() {
        /*
         * The complexity is O(N)
         */
        return size.get();
    }

    @Override
    public void clear() {
        /*
         * The complexity is O(1)
         */
        source.clear();
        size.set(0);
    }
}
