package org.evgeny.newsanalyser.storage;

import org.evgeny.newsanalyser.model.NewsItem;

import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * The implementation is very fast - O(1) - for adding and not so fast - O(N*log(N)) -
 * for getting m best items.
 */
public class ConcurrentQueueNewsItemStorage implements INewsItemStorage {

    final Comparator<NewsItem> comparator;

    ConcurrentLinkedQueue<NewsItem> source;

    public ConcurrentQueueNewsItemStorage(Comparator<NewsItem> comparator) {
        source = new ConcurrentLinkedQueue<>();
        this.comparator = comparator;
    }

    @Override
    public void add(NewsItem item) {
        /*
         * O(1) complexity
         */
        source.add(item);
    }

    @Override
    public List<NewsItem> getBestItems(int n) {
        /*
         * The complexity is O(N*log(N)) approximately. N is the size. The algorithm can
         * be implemented for O(N*n) complexity, using an ArrayList as a buffer
         * and removing the highest elements one by one, but it is not clear whether the
         * Constant under the hood of this way is really low. Besides the N should not be that
         * high to feel the difference.
         */
        List<NewsItem> res = new ArrayList<>();
        TreeSet<NewsItem> buffer = new TreeSet<>(comparator);
        buffer.addAll(source);
        // yes, this looks strange =) Since we do not filter the items while adding,
        // we do it here
        Set<Set<String>> headlines = new HashSet<>();
        Iterator<NewsItem> iterator = buffer.descendingIterator();

        while (iterator.hasNext() && res.size() < n) {
            NewsItem item = iterator.next();
            Set<String> headline = item.getHeadline();
            if (!headlines.contains(headline)) {
                headlines.add(headline);
                res.add(item);
            }
        }
        return res;
    }

    @Override
    public int getNumSeenItems() {
        /*
         * O(n) complexity
         */
        return source.size();
    }

    @Override
    public void clear() {
        /*
         * O(n) complexity
         */
        source.clear();
    }
}
