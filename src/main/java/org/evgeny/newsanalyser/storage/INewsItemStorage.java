package org.evgeny.newsanalyser.storage;

import org.evgeny.newsanalyser.model.NewsItem;

import java.util.List;

/**
 * The classes inheriting this interface must implement a data structure
 * which stores news items. The implementations must be thread-safe. The structure must
 * return only the elements with unique headlines via {@code getBestItems()}.
 * If there is an attempt to add an item with the headline already existing in the structure,
 * the only element that has the highest priority should remain.
 */
public interface INewsItemStorage {

    /**
     * adds a new item to the storage
     *
     * @param item to put
     */
    void add(NewsItem item);

    /**
     * Returns {@code n} best news items according to a rule in the sorted order. Best - first.
     *
     * @param n number to return
     * @return n best items
     */
    List<NewsItem> getBestItems(int n);

    /**
     * @return current number of all the items encountered (non unique)
     */
    int getNumSeenItems();

    /**
     * clear the structure
     */
    void clear();
}
