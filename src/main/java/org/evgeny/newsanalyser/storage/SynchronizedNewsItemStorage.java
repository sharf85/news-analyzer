package org.evgeny.newsanalyser.storage;

import org.evgeny.newsanalyser.model.NewsItem;

import java.util.*;

/**
 * An implementation of the {@link INewsItemStorage} encapsulating two data structures with
 * all the methods synchronized with each other
 */
public class SynchronizedNewsItemStorage implements INewsItemStorage {

    /**
     * There are two data structures under the hood. The newsItems is a tree set,
     * containing only "unique items" - the items with unique headlines and most priority.
     * The priorityByHeadline is a structure, which maps a headline to the current highest
     * priority.
     */
    final TreeSet<NewsItem> newsItems;

    final HashMap<Set<String>, Integer> priorityByHeadline;

    int size;

    public SynchronizedNewsItemStorage(Comparator<NewsItem> comparator) {
        this.newsItems = new TreeSet<>(comparator);
        this.priorityByHeadline = new HashMap<>();
        size = 0;
    }

    @Override
    public synchronized void add(NewsItem item) {
        /*
         * the method checks whether the headline was already stored, and if it really did,
         * compares the current priority with the previous one and updates, if necessary.
         * The complexity is O(N) in the worst case - if the headline already occurred,
         * but usually O(log(N)), where N is the size of newsItems.
         */
        Set<String> headline = item.getHeadline();
        Integer newPriority = item.getPriority();
        Integer oldPriority = priorityByHeadline.get(headline);

        if (oldPriority == null) {
            newsItems.add(item);
            priorityByHeadline.put(headline, newPriority);
        } else if (oldPriority < newPriority) {
            priorityByHeadline.put(headline, newPriority);

            newsItems.removeIf(itemToReplace -> headline.equals(itemToReplace.getHeadline()));
            newsItems.add(item);
        }

        size++;
    }

    @Override
    public synchronized List<NewsItem> getBestItems(int n) {
        /*
         * The complexity is O(n) approximately. n is the argument.
         */
        List<NewsItem> res = new ArrayList<>();
        Iterator<NewsItem> iterator = newsItems.descendingIterator();
        while (iterator.hasNext() && res.size() < n) {
            res.add(iterator.next());
        }

        return res;
    }

    @Override
    public synchronized int getNumSeenItems() {
        /*
         * The complexity is O(1)
         */
        return size;
    }

    public synchronized void clear() {
        /*
         * The complexity is O(N). N is the size. It can be decreased by creating a new instance of priorityByHeadline
         */
        newsItems.clear();
        priorityByHeadline.clear();
        size = 0;
    }
}
