package org.evgeny.newsanalyser.service;

import org.evgeny.newsanalyser.model.NewsItem;

/**
 * The Strategy which defines how much a NewsItem is positive
 */
public interface IPositivityStrategy {

    /**
     * Defines whether the item is positive
     *
     * @param item to estimate
     * @return whether the item is positive
     */
    public boolean isPositive(NewsItem item);

    /**
     * Estimates the rate of how much the item is positive
     *
     * @param item to estimate
     * @return value in percents
     */
    public double estimatePositivityOf(NewsItem item);
}
