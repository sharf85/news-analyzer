package org.evgeny.newsanalyser.service;

import org.evgeny.newsanalyser.model.NewsItem;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class DefaultPositivityStrategy implements IPositivityStrategy {

    final double positivityBarrier;

    final Set<String> positiveWords;

    public DefaultPositivityStrategy(
            @Value("#{'${org.evgeny.newsanalizer.default.positivitybarrier}'.split(',')}") double positivityBarrier,
            @Value("#{'${org.evgeny.newsanalizer.default.positivewords}'.split(',')}") Set<String> positiveWords) {
        this.positivityBarrier = positivityBarrier;
        this.positiveWords = positiveWords;
    }

    @Override
    public boolean isPositive(NewsItem item) {
        return estimatePositivityOf(item) > positivityBarrier;
    }

    @Override
    public double estimatePositivityOf(NewsItem item) {
        /*
         * estimation provides in percents
         */
        Set<String> headline = item.getHeadline();
        double size = headline.size();
        return 100 * headline.stream().filter(positiveWords::contains).count() / size;
    }
}
