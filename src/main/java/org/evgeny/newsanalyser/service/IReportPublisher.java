package org.evgeny.newsanalyser.service;

import org.evgeny.newsanalyser.model.NewsReport;

/**
 * Implementations of the interface must specify a rule how to publish reports.
 */
public interface IReportPublisher {

    void publish(NewsReport newsReport);
}
