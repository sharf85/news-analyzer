package org.evgeny.newsanalyser.service;

import org.evgeny.newsanalyser.model.NewsItem;
import org.evgeny.newsanalyser.model.NewsReport;
import org.evgeny.newsanalyser.storage.INewsItemStorage;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * The class performs main operations with News Items. The domain logic of the project.
 */
@Service
public class NewsItemService {

    private final INewsItemStorage storage;
    private final IPositivityStrategy positivity;

    /**
     * Since the INewsItemStorage implementations designed concurrent, the handling messages
     * threads (those which add new items to the item storage) can work concurrently relative
     * to each other, without mutual synchronization. But
     * when the report composing occurs, the program should freeze all the handling threads
     * and let the report thread work out.
     * <p>
     * ReadWriteLock class ideally fits for this case. It contains two locks, one of them
     * allows apply shared lock (reading lock) - meaning, the threads that accept items and then
     * handle them, work asynchronously. The second is a usual lock (writing lock),
     * which freezes all the handling threads when the reporting thread works and vice versa
     * freezes the reporting thread, if any of the handling threads works at the moment.
     */
    ReadWriteLock handleReportLock;
    Lock handleLock;
    Lock reportLock;

    public NewsItemService(INewsItemStorage storage,
                           IPositivityStrategy positivity) {
        this.storage = storage;
        this.positivity = positivity;

        handleReportLock = new ReentrantReadWriteLock();
        handleLock = handleReportLock.readLock();
        reportLock = handleReportLock.writeLock();
    }

    /**
     * The method handles another one incoming message. Adds the message to the item
     * storage if it's positive.
     *
     * @param headline the headline of the News Item
     * @param priority the priority of the News Item
     */
    public void handleItem(Collection<String> headline, int priority) {
        NewsItem item = new NewsItem(priority, headline);
        if (positivity.isPositive(item)) {
            handleLock.lock();
            try {
                storage.add(item);
            } finally {
                handleLock.unlock();
            }
        }
    }

    /**
     * The method forms the report about the best news items and number of the positive items.
     * Also cleans the storage before the next report.
     *
     * @return a report
     */
    public NewsReport composeReport(int positiveItemsNum) {
        reportLock.lock();
        try {
            NewsReport report = new NewsReport(
                    storage.getNumSeenItems(),
                    storage.getBestItems(positiveItemsNum));
            storage.clear();
            return report;
        } finally {
            reportLock.unlock();
        }
    }
}
