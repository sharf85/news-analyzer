package org.evgeny.newsanalyser.dto;

import java.util.List;

/**
 * The class is a typical 'date transfer object', used for wrapping incoming data
 * from TCP connections.
 */
public class NewsItemDto {

    public int priority;

    public List<String> headline;

}
