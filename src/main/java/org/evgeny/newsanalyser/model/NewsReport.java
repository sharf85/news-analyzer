package org.evgeny.newsanalyser.model;

import java.util.List;

public class NewsReport {
    int positiveItemsNum;
    List<NewsItem> bestItems;

    public NewsReport(int positiveItemsNum, List<NewsItem> bestItems) {
        this.positiveItemsNum = positiveItemsNum;
        this.bestItems = bestItems;
    }

    public int getPositiveItemsNum() {
        return positiveItemsNum;
    }

    public List<NewsItem> getBestItems() {
        return bestItems;
    }
}
