package org.evgeny.newsanalyser.model;

import org.evgeny.newsanalyser.service.IPositivityStrategy;
import org.springframework.stereotype.Component;

import java.util.Comparator;

/**
 * The default comparator of two News Items. Uses the following inverse rule
 * 	• First by priority, with highest priority first
 * 	• Then by positivity (percentage of words that are positive), with most positive first
 * 	• Then by number of words, with most words first
 * 	• Then by age, with the most recently received item first
 */
@Component
public class NewsItemDefaultComparator implements Comparator<NewsItem> {

    final IPositivityStrategy positivity;

    public NewsItemDefaultComparator(IPositivityStrategy positivity) {
        this.positivity = positivity;
    }

    @Override
    public int compare(NewsItem o1, NewsItem o2) {
        int res = Integer.compare(o1.getPriority(), o2.getPriority());
        if (res != 0)
            return res;

        res = Double.compare(
                positivity.estimatePositivityOf(o1),
                positivity.estimatePositivityOf(o2));
        if (res != 0)
            return res;

        res = Integer.compare(
                o1.getHeadline().size(),
                o2.getHeadline().size());
        if (res != 0)
            return res;

        res = o1.getReviewTime().compareTo(o2.getReviewTime());
        return res;
    }
}
