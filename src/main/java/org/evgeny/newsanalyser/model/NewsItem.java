package org.evgeny.newsanalyser.model;

import java.time.LocalDateTime;
import java.util.*;

public class NewsItem {

    /**
     * An integer value from the range [0..9]
     */
    private final int priority;

    /**
     * A set of words. It is designed unmodifiable, which is also allows using it as keys
     * for a HashMap, beside that it should not be modifiable.
     */
    private final Set<String> headline;

    /**
     * The time when the item was created. Depending on the business logic, may be opened
     * to change by the setter and, therefore, it will be not necessary to set the value in
     * the constructors
     */
    private final LocalDateTime reviewTime;

    public NewsItem(int priority, Collection<String> headline) {
        this.priority = priority;
        this.headline = Collections.unmodifiableSet(new HashSet<>(headline));
        reviewTime = LocalDateTime.now();
    }

    public NewsItem(int priority, String[] headline) {
        this(priority, Arrays.asList(headline));
    }

    public int getPriority() {
        return priority;
    }

    public Set<String> getHeadline() {
        return headline;
    }

    public LocalDateTime getReviewTime() {
        return reviewTime;
    }

}
