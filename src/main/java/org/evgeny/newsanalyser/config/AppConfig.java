package org.evgeny.newsanalyser.config;

import org.evgeny.newsanalyser.model.NewsItem;
import org.evgeny.newsanalyser.model.NewsItemDefaultComparator;
import org.evgeny.newsanalyser.service.IReportPublisher;
import org.evgeny.newsanalyser.storage.ConcurrentQueueNewsItemStorage;
import org.evgeny.newsanalyser.storage.INewsItemStorage;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {

    /**
     * An implementation of a console reporter.
     *
     * @return a bean that publishes reports.
     */
    @Bean
    public IReportPublisher consoleReportPublisher() {
        return (report) -> {
            StringBuilder builder = new StringBuilder();
            builder.append("The count of positive news items: ")
                    .append(report.getPositiveItemsNum())
                    .append("\nThe highest-priority positive news:");

            int i = 0;
            for (NewsItem item : report.getBestItems()) {
                builder.append("\n")
                        .append(++i)
                        .append(". ")
                        .append(String.join(", ", item.getHeadline()));
// The line below will add priority to the reports
//                builder.append(" (priority ").append(item.getPriority()).append(")");
            }
            builder.append("\n");
            System.out.println(builder);
        };
    }

    /**
     * An instance of the Data Structure for storing and getting information about the incoming
     * News Items
     *
     * @param comparator - the rule used to compare two News Items.
     * @return an instance of the {@link ConcurrentQueueNewsItemStorage} class. The implementation
     * is chosen, since it is very fast for saving new items (O(1)) and also concurrent.
     */
    @Bean
    public INewsItemStorage concurrentQueueNewsItemStorage(NewsItemDefaultComparator comparator) {
        return new ConcurrentQueueNewsItemStorage(comparator);
    }
}
