package org.evgeny.newsanalyser.config;

import org.evgeny.newsanalyser.dto.NewsItemDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.channel.ExecutorChannel;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.dsl.*;
import org.springframework.integration.ip.dsl.Tcp;
import org.springframework.integration.ip.tcp.connection.AbstractServerConnectionFactory;
import org.springframework.integration.ip.tcp.connection.TcpNetServerConnectionFactory;
import org.springframework.messaging.MessageChannel;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration
@EnableIntegration
public class TcpConfig {

    /**
     * Settings for the tcp connection. Set up as a server, based on the
     * {@link java.net.ServerSocket}
     */
    @Bean
    public AbstractServerConnectionFactory tcpConnectionFactory(
            @Value("${org.evgeny.newsanalyzer.tcp.server.port}") int port) {
        return new TcpNetServerConnectionFactory(port);
    }

//    @Bean
//    public MessageChannel fromTcp() {
//        return new DirectChannel();
//    }

    /**
     * The channel accepts messages from the TCP inbound adapter. Since the channel is based
     * on a Executor ({@link ExecutorChannel}), we can tune the executor for the amount of
     * threads used to accept the messages. Otherwise, if it was a {@link DirectChannel},
     * there would be as many threads as incoming tcp connections. Which one of them to use
     * is a matter of tuning.
     *
     * @return an instance of the channel between tcp inbound adapter and the transformer
     * of incoming data to NewsItem objects.
     */
    @Bean
    public MessageChannel fromTcp() {
        return new ExecutorChannel(fromTcpExecutor());
    }

    /**
     * The channel is between the data->NewsItemDto transformer and the message consuming
     * endpoint. Performs in the same thread, which was used to pass the data from TCP
     * adapter to the transformer. Meaning, the messages are still handled in the threads
     * from {@code fromTcpExecutor()}
     */
    @Bean
    public MessageChannel fromTcpTransformed() {
        return new DirectChannel();
    }

    /**
     * @return an executor instance, the threads of which are used for accepting data from tcp
     * and also handling it up to the NewsItem storage.
     */
    @Bean
    public TaskExecutor fromTcpExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(5);
        executor.setMaxPoolSize(10);
        return executor;
    }

    /**
     * @param tcpConnectionFactory - tcp connection factory
     * @return an instance describing the flow from accepting data from tcp and up to the
     * consuming endpoint, listening to the "fromTcpTransformed" channel. See
     * {@link org.evgeny.newsanalyser.NewsAnalyserApplication}
     */
    @Bean
    public IntegrationFlow tcpIn(AbstractServerConnectionFactory tcpConnectionFactory) {
        return IntegrationFlows
                .from(Tcp.inboundAdapter(tcpConnectionFactory))
                .channel("fromTcp")
                .transform(Transformers.fromJson(NewsItemDto.class))
                .channel("fromTcpTransformed")
                .get();
    }

}
