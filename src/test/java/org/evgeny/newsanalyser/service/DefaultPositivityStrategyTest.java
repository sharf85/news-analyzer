package org.evgeny.newsanalyser.service;

import org.evgeny.newsanalyser.model.NewsItem;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class DefaultPositivityStrategyTest {
    DefaultPositivityStrategy strategy;

    Set<String> positiveWords = new HashSet<>(Arrays.asList("word1", "word2", "word3", "word4"));


    @BeforeEach
    public void init() {
        strategy = new DefaultPositivityStrategy(50, positiveWords);
    }

    double delta = 0.00001;

    @Test
    public void testEstimatePositivityOf_onePositiveWordOfOne_positivity100() {
        NewsItem item = new NewsItem(0, Arrays.asList("word1"));
        assertEquals(100, strategy.estimatePositivityOf(item), delta);
    }

    @Test
    public void testEstimatePositivityOf_oneNonPositiveWordOfOne_positivity0() {
        NewsItem item = new NewsItem(0, Arrays.asList("some"));
        assertEquals(0, strategy.estimatePositivityOf(item), delta);
    }

    @Test
    public void testEstimatePositivityOf_oneNonPositiveAndOnePositive_positivity50() {
        NewsItem item = new NewsItem(0, Arrays.asList("some", "word1"));
        assertEquals(50, strategy.estimatePositivityOf(item), delta);
    }

    @Test
    public void testEstimatePositivityOf_twoPositives_positivity100() {
        NewsItem item = new NewsItem(0, Arrays.asList("word2", "word1"));
        assertEquals(100, strategy.estimatePositivityOf(item), delta);
    }

    @Test
    public void testEstimatePositivityOf_twoPositivesAndOneNot_positivity2Over3() {
        NewsItem item = new NewsItem(0, Arrays.asList("word2", "some", "word1"));
        assertEquals(66.66666, strategy.estimatePositivityOf(item), delta);
    }

    @Test
    public void testEstimatePositivityOf_onePositiveAndTwoNot_oneThird() {
        NewsItem item = new NewsItem(0, Arrays.asList("word2", "some", "some2"));
        assertEquals(33.33333, strategy.estimatePositivityOf(item), delta);
    }

    @Test
    public void testEstimatePositivityOf_twoPositiveAndTwoNot_positivity50() {
        NewsItem item = new NewsItem(0, Arrays.asList("word2", "some", "word3", "some2"));
        assertEquals(50, strategy.estimatePositivityOf(item), delta);
    }

    @Test
    public void testEstimatePositivityOf_threePositiveAndOneNot_positivity75() {
        NewsItem item = new NewsItem(0, Arrays.asList("word2", "some", "word4", "word3"));
        assertEquals(75, strategy.estimatePositivityOf(item), delta);
    }

    @Test
    public void testIsPositive_onePositiveWordOfOne_positive() {
        NewsItem item = new NewsItem(0, Arrays.asList("word1"));
        assertTrue(strategy.isPositive(item));
    }

    @Test
    public void testIsPositive_oneNonPositiveWordOfOne_nonPositive() {
        NewsItem item = new NewsItem(0, Arrays.asList("some"));
        assertFalse(strategy.isPositive(item));
    }

    @Test
    public void testIsPositive_oneNonPositiveAndOnePositive_nonPositive() {
        NewsItem item = new NewsItem(0, Arrays.asList("some", "word1"));
        assertFalse(strategy.isPositive(item));
    }

    @Test
    public void testIsPositive_twoPositives_positive() {
        NewsItem item = new NewsItem(0, Arrays.asList("word2", "word1"));
        assertTrue(strategy.isPositive(item));
    }

    @Test
    public void testIsPositive_twoPositivesAndOneNot_positive() {
        NewsItem item = new NewsItem(0, Arrays.asList("word2", "some", "word1"));
        assertTrue(strategy.isPositive(item));
    }

    @Test
    public void testIsPositive_onePositiveAndTwoNot_nonPositive() {
        NewsItem item = new NewsItem(0, Arrays.asList("word2", "some", "some2"));
        assertFalse(strategy.isPositive(item));
    }

    @Test
    public void testIsPositive_twoPositiveAndTwoNot_nonPositive() {
        NewsItem item = new NewsItem(0, Arrays.asList("word2", "some", "word3", "some2"));
        assertFalse(strategy.isPositive(item));
    }

    @Test
    public void testIsPositive_threePositiveAndOneNot_positive() {
        NewsItem item = new NewsItem(0, Arrays.asList("word2", "some", "word4", "word3"));
        assertTrue(strategy.isPositive(item));
    }
}
