package org.evgeny.newsanalyser.storage;

import org.junit.jupiter.api.BeforeEach;

class SynchronizedNewsItemStorageTest extends INewsItemStorageTest {
    @BeforeEach
    public void init() {
        super.init();
        storage = new SynchronizedNewsItemStorage(comparator);
    }
}
