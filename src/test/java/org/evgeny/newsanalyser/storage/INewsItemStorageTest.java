package org.evgeny.newsanalyser.storage;

import org.evgeny.newsanalyser.model.NewsItem;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
abstract class INewsItemStorageTest {

    NewsItem item1 = new NewsItem(1, Arrays.asList("1", "2", "3"));
    NewsItem item2 = new NewsItem(2, Arrays.asList("1", "2"));
    NewsItem item3 = new NewsItem(3, Arrays.asList("1", "3"));
    NewsItem item4 = new NewsItem(2, Arrays.asList("1", "2", "3"));
    NewsItem item5 = new NewsItem(3, Arrays.asList("1", "2"));

    @BeforeEach
    public void init() {

        lenient().when(comparator.compare(item1, item1)).thenReturn(0);
        lenient().when(comparator.compare(item2, item2)).thenReturn(0);
        lenient().when(comparator.compare(item3, item3)).thenReturn(0);
        lenient().when(comparator.compare(item4, item4)).thenReturn(0);
        lenient().when(comparator.compare(item5, item5)).thenReturn(0);

        lenient().when(comparator.compare(item1, item2)).thenReturn(-1);
        lenient().when(comparator.compare(item2, item1)).thenReturn(1);
        lenient().when(comparator.compare(item1, item3)).thenReturn(-1);
        lenient().when(comparator.compare(item3, item1)).thenReturn(1);
        lenient().when(comparator.compare(item1, item4)).thenReturn(-1);
        lenient().when(comparator.compare(item4, item1)).thenReturn(1);
        lenient().when(comparator.compare(item1, item5)).thenReturn(-1);
        lenient().when(comparator.compare(item5, item1)).thenReturn(1);

        lenient().when(comparator.compare(item2, item3)).thenReturn(-1);
        lenient().when(comparator.compare(item3, item2)).thenReturn(1);
        lenient().when(comparator.compare(item2, item4)).thenReturn(-1);
        lenient().when(comparator.compare(item4, item2)).thenReturn(1);
        lenient().when(comparator.compare(item2, item5)).thenReturn(-1);
        lenient().when(comparator.compare(item5, item2)).thenReturn(1);

        lenient().when(comparator.compare(item3, item4)).thenReturn(1);
        lenient().when(comparator.compare(item4, item3)).thenReturn(-1);
        lenient().when(comparator.compare(item3, item5)).thenReturn(-1);
        lenient().when(comparator.compare(item5, item3)).thenReturn(1);

        lenient().when(comparator.compare(item4, item5)).thenReturn(-1);
        lenient().when(comparator.compare(item5, item4)).thenReturn(1);
    }

    INewsItemStorage storage;

    /**
     * Strictly speaking, the comparator can be any. The INewsItemStorage implementations
     * must not be dependant on the comparator rules, take into consideration the uniqueness
     * of the headlines
     */
    @Mock
    Comparator<NewsItem> comparator;

    @Test
    public void testGetNumSeenItems_3uniqueItemsFrom3_3() {
        storage.add(item1);
        storage.add(item2);
        storage.add(item3);
        assertEquals(3, storage.getNumSeenItems());
    }

    @Test
    public void testGetNumSeenItems_3UniqueFrom4_4() {
        storage.add(item1);
        storage.add(item2);
        storage.add(item3);
        storage.add(item4);

        assertEquals(4, storage.getNumSeenItems());
    }

    @Test
    public void testGetBestItems_3UniqueFrom3And3ItemsToGet_3Items() {
        storage.add(item1);
        storage.add(item2);
        storage.add(item3);

        List<NewsItem> items = storage.getBestItems(3);
        assertEquals(3, items.size());
        assertEquals(Arrays.asList(item3, item2, item1), items);
    }

    @Test
    public void testGetBestItems_3UniqueFrom4And3ItemsToGet_3Items() {
        storage.add(item1);
        storage.add(item2);
        storage.add(item3);
        storage.add(item4);

        List<NewsItem> items = storage.getBestItems(3);
        assertEquals(3, items.size());
        assertEquals(Arrays.asList(item3, item4, item2), items);
    }

    @Test
    public void testGetBestItems_3UniqueFrom4And4ItemsToGet_3Items() {
        storage.add(item1);
        storage.add(item2);
        storage.add(item3);
        storage.add(item4);

        List<NewsItem> items = storage.getBestItems(4);
        assertEquals(3, items.size());
        assertEquals(Arrays.asList(item3, item4, item2), items);
    }

    @Test
    public void testGetBestItems_3UniqueFrom4And5ItemsToGet_3Items() {
        storage.add(item1);
        storage.add(item2);
        storage.add(item3);
        storage.add(item4);

        List<NewsItem> items = storage.getBestItems(5);
        assertEquals(3, items.size());
        assertEquals(Arrays.asList(item3, item4, item2), items);
    }

    @Test
    public void testGetBestItems_3UniqueFrom5And2ItemsToGet_2Items() {
        storage.add(item1);
        storage.add(item2);
        storage.add(item3);
        storage.add(item4);
        storage.add(item5);

        List<NewsItem> items = storage.getBestItems(2);
        assertEquals(2, items.size());
        assertEquals(Arrays.asList(item5, item3), items);
    }

    @Test
    public void testGetBestItems_3UniqueFrom5And3ItemsToGet_3Items() {
        storage.add(item1);
        storage.add(item2);
        storage.add(item3);
        storage.add(item4);
        storage.add(item5);

        List<NewsItem> items = storage.getBestItems(3);
        assertEquals(3, items.size());
        assertEquals(Arrays.asList(item5, item3, item4), items);
    }

    @Test
    public void testGetBestItems_3UniqueFrom5And4ItemsToGet_3Items() {
        storage.add(item1);
        storage.add(item2);
        storage.add(item3);
        storage.add(item4);
        storage.add(item5);

        List<NewsItem> items = storage.getBestItems(4);
        assertEquals(3, items.size());
        assertEquals(Arrays.asList(item5, item3, item4), items);
    }

    @Test
    public void testGetBestItems_3UniqueFrom5And5ItemsToGet_3Items() {
        storage.add(item1);
        storage.add(item2);
        storage.add(item3);
        storage.add(item4);
        storage.add(item5);

        List<NewsItem> items = storage.getBestItems(5);
        assertEquals(3, items.size());
        assertEquals(Arrays.asList(item5, item3, item4), items);
    }

    @Test
    public void testAdd_5ConcurrentThreads_correctSize() throws InterruptedException {
        ExecutorService executor = Executors.newFixedThreadPool(5);

        Callable<Void> task = () -> {
            storage.add(item1);
            storage.add(item2);
            storage.add(item3);
            storage.add(item4);
            storage.add(item5);
            return null;
        };

        List<Callable<Void>> tasks = Collections.nCopies(10000, task);
/*
 The estimation of the running time from below is not correct, since we have only 5 different
 items
 */

//        long startExecution = System.currentTimeMillis();
        executor.invokeAll(tasks);
//        System.out.println(System.currentTimeMillis() - startExecution);

        assertEquals(50000, storage.getNumSeenItems());
    }
}
