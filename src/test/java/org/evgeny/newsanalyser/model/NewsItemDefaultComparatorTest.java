package org.evgeny.newsanalyser.model;

import org.evgeny.newsanalyser.service.IPositivityStrategy;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class NewsItemDefaultComparatorTest {

    @Mock
    IPositivityStrategy positivityStrategy;

    @InjectMocks
    NewsItemDefaultComparator comparator;

    @Test
    public void testCompare_differentPriority() {
        NewsItem item1 = new NewsItem(3, Collections.emptyList());
        NewsItem item2 = new NewsItem(4, Collections.emptyList());

        assertTrue(comparator.compare(item1, item2) < 0);
        assertTrue(comparator.compare(item2, item1) > 0);
    }

    @Test
    public void testCompare_samePriorityDifferentPositivity() {
        NewsItem item1 = new NewsItem(4, Collections.emptyList());
        NewsItem item2 = new NewsItem(4, Collections.emptyList());

        when(positivityStrategy.estimatePositivityOf(item1)).thenReturn(50d);
        when(positivityStrategy.estimatePositivityOf(item2)).thenReturn(60d);

        assertTrue(comparator.compare(item1, item2) < 0);
        assertTrue(comparator.compare(item2, item1) > 0);
    }

    @Test
    public void testCompare_samePriorityAndPositivityDifferentWordsNumber() {
        NewsItem item1 = new NewsItem(4, Arrays.asList("1", "2", "3"));
        NewsItem item2 = new NewsItem(4, Arrays.asList("1", "2", "3", "4"));

        when(positivityStrategy.estimatePositivityOf(item1)).thenReturn(60d);
        when(positivityStrategy.estimatePositivityOf(item2)).thenReturn(60d);

        assertTrue(comparator.compare(item1, item2) < 0);
        assertTrue(comparator.compare(item2, item1) > 0);
    }

    @Test
    public void testCompare_samePriorityAndPositivityAndWordsNumberDifferentCreationTime() throws InterruptedException {
        NewsItem item1 = new NewsItem(4, Arrays.asList("1", "2", "3"));
        Thread.sleep(1);
        NewsItem item2 = new NewsItem(4, Arrays.asList("1", "2", "3"));

        when(positivityStrategy.estimatePositivityOf(item1)).thenReturn(60d);
        when(positivityStrategy.estimatePositivityOf(item2)).thenReturn(60d);

        assertTrue(comparator.compare(item1, item2) < 0);
        assertTrue(comparator.compare(item2, item1) > 0);
    }

}
