# README #

### The News Flow Project ###

* The project consists of two parts: a "Mock News Feed" and a "News Analyser". Several instances of the Mock News Feed can be run simultaneously, each connecting to the same News Analyser.
* The Mock News Feed should periodically generate messages containing random News Item.
* The News Analyser should handle the messages from the news feeds and periodically display a short summary about news items that are considered "interesting".

### News Analyser ###
The current repository is for the News Analyser part of the project.

* Listens on a TCP port for connections from Mock News Feeds and receives news item messages.
* Inspects the news item headlines and decides whether they are overall positive or negative. If more than 50% of words in the headline are positive ("up", "rise", "good", "success", "high" or "über"), the news item as a whole is considered positive. Negative news items are ignored by the analyser.
* Every 10 seconds, the News Analyser should output to the console:
    * the count of positive news items seen during the last 10 seconds
    * the unique headlines of up to three of the highest-priority positive news items seen during the last 10 seconds

### Setup  ###

* The project consists of two repositories: Mock News Feed and News Analyser. Each of them is a Spring Boot project on Gradle.
* They can be run by opening in an IDE or directly via Gradle build tool from the console. [Official Gradle documentation](https://docs.gradle.org).
* The application properties can be found by the following path:  
`
<project root>/src/main/resources/application.properties
`
* The port of the TCP server is `3000` by default - can be easily changed onto an appropriate one in the properties files. TCP connection was established via [Spring Integration](https://docs.spring.io/spring-integration/docs/5.3.1.RELEASE/reference/html/).
